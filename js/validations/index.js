export const validateCharLength = (pass) => {
  if( pass.length >= 8 ) {
      return true;
  }
}

export const validateUppercase = (pass) => {
  var regex = /^(?=.*[A-Z]).+$/; // Uppercase character pattern

  if( regex.test(pass) ) {
      return true;
  }
}

export const validateNumber = (pass) => {
  var regex = /^(?=.*[0-9]).+$/; // number pattern

  if( regex.test(pass) ) {
      return true;
  }
}

function escapeRegExp(string){
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

const fieldPassword = document.querySelector('#password');
const fieldConfirmPassword = document.querySelector('#confirm_password');

fieldPassword.addEventListener("keyup", function(e) {
  this.setCustomValidity(this.validity.patternMismatch ? fieldPassword.title : "");
  if(this.checkValidity()) {
    fieldConfirmPassword.pattern = escapeRegExp(this.value);
    fieldConfirmPassword.setCustomValidity(fieldConfirmPassword.title);
  } else {
    fieldConfirmPassword.pattern = this.pattern;
    fieldConfirmPassword.setCustomValidity("");
  }
}, false);

fieldConfirmPassword.addEventListener("keyup", function(e) {
  this.setCustomValidity(this.validity.patternMismatch ? fieldConfirmPassword.title : "");
}, false);