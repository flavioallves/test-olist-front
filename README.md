![Logo of the project](./img/logotype.png)

# Test Olist front

This project is an opportunity to show my skills with front end as per the requirements and I apply for the opportunity to work on [Olist](https://olist.com/).

## Getting started

To run the project and run tests.

```shell
yarn dev
```
then run:
```shell
python -m SimpleHTTPServer 8080
```
first command will start the webpack, responsible for generating an application bundle.
second will initialize the local server to run the application.

## Developing

### Built With
javascript, babel and webpack.

### Prerequisites
it is necessary to have node.js installed and yarn.

### Work environment used to run this project
It was used: (MacBook / macOS Mojave, VS IDE code, Node.js and Yarn)


### Setting up Dev
To start developing the project:

```shell
git clone git@gitlab.com:FlavioENE/test-olist-front.git
cd test-olist-front/
yarn install
```
The above commands will download the project and install all dependencies.
then you start the webpack with `yarn dev` command and then boot a local server of your choice within the project root.
local server suggestion `python -m SimpleHTTPServer 8080`

### Deploying / Publishing
Project is deploy automatically, when making changes in the source and sending to the [repository](https://gitlab.com/FlavioENE/test-olist-front), netlify is integrated with [Gitlab](https://gitlab.com/) so that every new version is deployed.
